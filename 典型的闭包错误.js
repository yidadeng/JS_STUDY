// function test() {

//     var arr = [];
//     for (var i = 0; i < 10; i++) {
//         arr[i] = function () {
//             console.log(i);
//         }
//     }
//     return arr
// }

// var myArr = test();

// for (var j = 0; j < 10; j++) {
//     myArr[j]();
// }

// ==>> 10 10 10 10 10 10 10 10 10 10


// 上面这个问题可以用立即执行函数来解决

function test(){
    var arr = [];
    for(var i = 0;i < 10 ;i ++){
        (function(n) {
            arr[n] = function(){
                console.log(n);
            }
        }(i));
    }
    return arr;
}

var myArr = test();

for(var j = 0; j<10 ; j++){
    myArr[j]();
}
// ==>> 0 1 2 3 4 5 6 7 8 9




//也可以将上面循环里的var 变成let来解决 i 变量作用域提升的问题

function test() {

    var arr = [];
    for (let i = 0; i < 10; i++) {
        arr[i] = function () {
            console.log(i);
        }
    }
    return arr
}

var myArr = test();

for (var j = 0; j < 10; j++) {
    myArr[j]();
}
// ==>> 0 1 2 3 4 5 6 7 8 9




