// Array.prototype.filter()
//  filter() 方法创建一个新数组, 其包含通过所提供函数实现的测试的所有元素。



var words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
var result = words.filter(word => word.length > 6);
// 上面的箭头函数也可以写成这样
// var result = words.filter( function(ele){
//   return ele.length > 6;
// })


//筛选排除所有较小的值
function isBigEnough(ele){
  return ele >= 10;
}

var bigArray = [12, 5, 8, 130, 44].filter(isBigEnough);




// 过滤 JSON 中的无效条目

var arr =[
  { id: 15 },
  { id: -1 },
  { id: 0 },
  { id: 3 },
  { id: 12.2 },
  { },
  { id: null },
  { id: NaN },
  { id: 'undefined' }
]

var invalidEntries = 0;

function isNumber(num){
  return num !==undefined && typeof(num) === 'number' && !isNaN(num);
}

function filterById(item){
  if(isNumber(item.id) && item.id !== 0){
    return true;
  }
  invalidEntries ++
  return false;
}

var newArr = arr.filter(filterById).sort(function(a,b){
  return a.id - b.id
});

console.log(newArr);
console.log(`Number of Invalid Entries = ${invalidEntries}`);


// 在数组中搜索
var fruits = ['apple', 'banana', 'grapes', 'mango', 'orange'];

function filterItem(query){
  var resArray = fruits.filter(function(el){
    return  el.toLowerCase().indexOf(query.toLowerCase()) > -1;
  })
  if(resArray.length <= 0){
    resArray.push("没有符合条件的东西")
  }
  return resArray;
}

console.log(filterItem('deefg'))  //  在数组中搜索'deefg'