
// 封装绑定函数

function addEvent(elem, type, handle) {
    if(elem.addEventListener){
        elem.addEventListener(type, handel, false)
    }else if(elem.attachEvent){
        elem.attachEvent('on' + type, function(){
            handle.call(elem);
        })
    }else{
        elem['on' + type] = handle;
    }
}


// 解除绑定函数
ele.onclick = false/null/''
ele.removeEventListener(type, fn, false)
ele.detachEvent('on' + type, fn)