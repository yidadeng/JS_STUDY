let obj = {
  a: true,
  b: true
}


let objProxy = new Proxy(obj, {
  get(target, key) {
    console.log(target, key)
    return target[key]
  },
  set(target, key, value) {
    console.log(key, value)
    target[key] = value;
  }
})

//  显示属性a
objProxy.a

//  修改属性a
objProxy.a = false

//  增加属性c

objProxy.c = false