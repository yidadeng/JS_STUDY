// Array.prototype.map()
//map() 方法创建一个新数组，其结果是该数组中的每个元素都调用一个提供的函数后返回的结果。



//使用 map 重新格式化数组中的对象

var kvArray = [{
  key: 1,
  value: 10
},
{
  key: 2,
  value: 20
},
{
  key: 3,
  value: 30
}
]

var newArray = kvArray.map(function (obj) {
var rObj = {};
rObj[obj.key] = obj.value
return rObj
})

console.log(newArray)




//  使用一个包含一个参数的函数来mapping(构建)一个数字数组
var array01 = [1,2,3,4];
var array02 = array01.map(function(num){
return num * 2
})

console.log(array02);



//  一般的map 方法
// 下面的例子演示如何在一个 String  上使用 map 方法获取字符串中每个字符所对应的 ASCII 码组成的数组：
var map = Array.prototype.map;
var sentence = "This is an apple";
var a = map.call(sentence , function(x){
 //return  x.charCodeAt(0);
 var senObj = {}
 senObj[x] = x.charCodeAt();
 senObj["目标字母"] = x.charAt();
 senObj["目标字母ASCII码值"] = x.charCodeAt();
 return senObj;
})


console.log(a);


// querySelectorAll 应用
// 这里，我们获得了文档里所有选中的选项，并将其打印

var  elems = document.querySelectorAll("select option:checked");
var value = Array.prototype.map.call(elems , function(obj){
return obj.value;
})

