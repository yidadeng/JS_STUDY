function deepClone(origin, target) {
  var target = target || {},
    toStr = Object.prototype.toString,
    arrStr = '[object Array]';
  for (var prop in origin) {
    if (origin.hasOwnProperty(prop)) { //只复制其本身所拥有的属性
      if (origin[prop] !== 'null' && typeof (origin[prop]) == "object") {
        // 数组类型和对象类型判断后进行复制  
        if (toStr.call(origin[prop]) == arrStr) {
          target[prop] == [];
        } else {
          target[prop] == {};
        }
        //递归
        deepClone(origin[prop], target[prop]);
      } else {
        // 简单类型直接复制  
        target[prop] = origin[prop];
      }
    }
  }
  return target;
}


var yuanlai = {
  name : 'shuiguo',
  price: 4,
  shenfen :{
    he : 4,
    lae :'suibian'
  }
}
var mubiao = {}

deepClone(yuanlai , mubiao)