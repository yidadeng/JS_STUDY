//////////////////////////////////////////①

Person.prototype = {
    Height: 100
}

function Person() {
    this.eat = function () {
        this.Height++
    }
}

var person = new Person();


person.eat();



//////////////////////////////////////////②


obj = Object.create()



////////////////获得随机数//////////////////////////③

var num = Math.floor(Math.random() * 100);
console.log(num)


///////call & apply 作用都是改变this指向，区别就是传参列表不同////////////////////////////////////////④

function Person(name, age) {
    // this == obj 原本这里的this指向 window，后来被.call改变了，指向了obj
    this.name = name,
    this.age = age
}
var person = new Person('deng', 100);
var obj = {

};
Person.call(obj, 'cheng', 300);
////call 的第一位参数会改变this的指向，后面的参数会作为正常的实参去传到形参里面去。


//////////////⑤

function Person(name , age ,sex){
    this.name = name;
    this.age = age;
    this.sex = sex;
}
function Student (name,age,sex,tel,grade){
    //var this = {}
     Person.call(this , name , age ,sex);
     //Person.apply(this , [name , age ,sex])
     // call 需要把实参按照形参的个数传进去
     // apply 需要传一个arguments ,通常是数组
     this.tel = tel;
     this.grade = grade;
}


var student = new Student('Sunny' , 18 , 'male' , 1396 ,2019);



//////////////  6
 

